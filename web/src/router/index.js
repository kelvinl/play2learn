import Vue from 'vue'
import Router from 'vue-router'
import CourseList from '@/components/CourseList'
import CourseDetail from '@/components/CourseDetail'
import Report from '@/components/Report'
import Notes from '@/components/Notes'
import Assignment from '@/components/Assignment'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/student/:sid',
      name: 'COURSELIST',
      component: CourseList
    },
    {
      path: '/notes/:sid/:cid',
      component: CourseDetail,
      children: [
        {
          path: '/notes/:sid/:cid',
          name: 'NOTES',
          component: Notes
        },
        {
          path: '/report/:sid/:cid',
          name: 'REPORT',
          component: Report
        },
        {
          path: '/assignment/:sid/:cid',
          name: 'ASSIGNMENT',
          component: Assignment
        }
      ]
    }
  ]
})
