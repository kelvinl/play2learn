import Vue from 'vue'
import Router from 'vue-router'

import ReportList from '@/components/ReportList'
import Report from '@/components/Report'
import Student from '@/components/Student'
import StudentList from '@/components/StudentList'
import Course from '@/components/Course'
import CourseList from '@/components/CourseList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: ReportList
    },
    {
      path: '/addReport',
      name: 'AddReport',
      component: Report
    },
    {
      path: '/editReport/:id',
      component: Report
    },
    {
      path: '/studentList',
      component: StudentList
    },
    {
      path: '/editStudent/:id',
      component: Student
    },
    {
      path: '/addStudent',
      component: Student
    },
    {
      path: '/courseList',
      component: CourseList
    },
    {
      path: '/editCourse/:id',
      component: Course
    },
    {
      path: '/addCourse',
      component: Course
    }
  ]
})
