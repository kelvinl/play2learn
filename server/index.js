const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const api = require('./api')
const path = require('path')
const PORT = process.env.PORT || 8080

function errorHandler(err, req, res, next) {
  res.status(500).send(err.message)
}

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/api', api)
app.use('/storage', express.static(path.join(__dirname, 'public')))

app.use(errorHandler)

app.listen(PORT)
console.log('Now running on http://localhost:' + PORT)
