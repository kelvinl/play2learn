const mongoose = require('mongoose')
const Schema = mongoose.Schema

/* ------------------------------ 学生信息 ------------------------------ */

const studentSchema = new Schema(
  {
    name: String,
    age: { type: Number, min: 0 },
    sid: String,
    district: String,
    gender: Number,
    desc: String
  },
  {
    id: false,
    toJSON: { virtuals: true }
  }
)

studentSchema.virtual('courses', {
  ref: 'Course',
  localField: '_id',
  foreignField: 'students'
})

// 必须是 doc 才能触发 remove
studentSchema.pre('remove', async function(next) {
  try {
    await this.model('Course').update(
      {},
      { $pull: { students: this._id } },
      { multi: true }
    ).exec()
    await this.model('Report').remove({ students: this._id })
    next()
  } catch(err) { console.error(err) }
})

const Student = mongoose.model('Student', studentSchema)

/* ------------------------------ 课程信息 ------------------------------ */

const courseSchema = new Schema({
  name: String,
  desc: String,
  date: { type: Date, default: Date.now },
  students: [{ type: Schema.Types.ObjectId, ref: 'Student' }],
  notes: [{
    title: String,
    steps: [{
      title: String,
      desc: String,
      pic: String,
      video: String
    }]
  }],
  available: { type: Boolean, default: false }
})

// 必须是 doc 才能触发 remove
courseSchema.pre('remove', async function(next) {
  try {
    await this.model('Report').remove({ course: this._id })
    next()
  } catch(err) { console.error(err) }
})

const Course = mongoose.model('Course', courseSchema)

/* ------------------------------ 评价信息 ------------------------------ */

const reportSchema = new Schema({
  course: { type: Schema.Types.ObjectId, ref: 'Course' },
  student: { type: Schema.Types.ObjectId, ref: 'Student' },
  evaluated: { type: Boolean, default: false },
  badges: [{
    _id: false,
    name: String,
    alias: String,
    count: Number
  }],
  comment: String,
  assignment: String
})

const Report = mongoose.model('Report', reportSchema)

module.exports = { Student, Course, Report }
