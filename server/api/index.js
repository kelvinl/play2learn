const express = require('express')
const routes = express.Router()
const course = require('./course')
const student = require('./student')
const report = require('./report')
const utils = require('./utils')

const mongoose = require('mongoose')
mongoose.Promise = global.Promise

mongoose.connect('mongodb://mongo/courses')

const db = mongoose.connection

db.on('error', err => console.error('connection error: ' + err))
db.once('open', () => console.log('mongoose is connecting with your mongodb...'))

routes.use('/course', course)
routes.use('/student', student)
routes.use('/report', report)
routes.use('/utils', utils)

module.exports = routes
