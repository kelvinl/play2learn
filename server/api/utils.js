// 各种功能接口
const express = require('express')
const routes = express.Router()

const qrCode = require('qrcode')

const multer = require('multer')
const BASE_UPLOAD_PATH = 'public/pics'
const uploadNotePic = multer({ dest: `${BASE_UPLOAD_PATH}/note/` })

/* ------------------------------ 二维码 ------------------------------ */

routes.get('/getQRCode', (req, res) => {
  const QR_CODE_BASE_URL = 'http://h5.youlaoshi.org/#/student'
  // const QR_CODE_BASE_URL = 'http://192.168.0.105:3300/#/student'
  qrCode.toDataURL(`${QR_CODE_BASE_URL}/${req.query.sid}`, { errorCorrectionLevel: 'M' }, (err, url) => {
    if (err) {
      res.status(400).send(err.message)
    } else {
      res.status(200).send(url)
    }
  })
})

/* ------------------------------ 上传图片 ------------------------------ */

routes.post('/upload/notePic', uploadNotePic.single('notePic'), (req, res) => {
  const resData = {
    picName: req.file.filename
  }
  res.status(200).send(JSON.stringify(resData))
})

module.exports = routes
