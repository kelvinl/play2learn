// 评价接口
const express = require('express')
const routes = express.Router()
const { Student, Report } = require('./models')

routes.post('/add', async(req, res) => {
  try {
    await new Report(req.body).save()
    res.status(200).send('Report added successfully')
  } catch(err) { res.status(400).send(err.message) }
})

routes.delete('/delete', async(req, res) => {
  try {
    const report = await Report.findById(req.query._id).exec()
    if (report) {
      await report.remove()
      res.status(200).send('Report deleted')
    } else {
      res.status(404).send('Error: Report not found')
    }
  } catch(err) { res.status(400).send(err.message) }
})

routes.put('/update', async(req, res) => {
  try {
    await Report.update({ _id: req.body._id }, req.body).exec()
    res.status(200).send('Report updated')
  } catch(err) { res.status(404).send('Error: Report not found') }
})

routes.get('/getReportByIds', async(req, res) => {
  try {
    const report = await Report.findOne({ student: req.query.student, course: req.query.course }).exec()
    report ? res.send(JSON.stringify(report)) : res.status(404).send('Error: Report not found')
  } catch(err) { res.status(400).send(err.message) }
})

routes.get('/getReport', async(req, res) => {
  try {
    const report = await Report.findById(req.query._id)
    .populate('student', 'name gender age desc').populate('course', 'name desc').exec()
    report ? res.send(JSON.stringify(report)) : res.status(404).send('Error: Report not found')
  } catch(err) { res.status(400).send(err.message) }
})

routes.get('/getReports', async(req, res) => {
  try {
    const reports = await Report.find().exec()
    reports ? res.send(JSON.stringify(reports)) : res.status(404).send('Error: Reports not found')
  } catch(err) { res.status(400).send(err.message) }
})

routes.get('/getReportList', async(req, res) => {
  try {
    const reports = await Report.find().exec()
    const students = await Student.find({}, 'name district').populate({
      path: 'courses evaluated',
      select: 'name',
      populate: { path: 'reports', select: 'evaluated' }
    }).exec()
    if (students) {
      let courses = []
      students.forEach(student => {
        student.courses.forEach(course => {
          let evaluated, rid
          reports.forEach(report => {
            // 转成 String 才能比较
            if (String(report.course) == String(course._id) && String(report.student) == String(student._id)) {
              evaluated = true
              rid = report._id
            }
          })
          courses.push({
            evaluated,
            rid,
            sid: student._id,
            studentName: student.name,
            district: student.district,
            cid: course._id,
            courseName: course.name
          })
        })
      })
      res.send(JSON.stringify(courses))
    } else { res.status(404).send('Error: Student not found') }
  } catch(err) { res.status(400).send(err.message) }
})

module.exports = routes
