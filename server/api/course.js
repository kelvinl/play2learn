// 课程接口
const express = require('express')
const routes = express.Router()
const { Student, Course } = require('./models')

// 要先把已存在的 student._id 录入
routes.post('/add', async(req, res) => {
  try {
    const students = await Student.find({}, '_id').exec()
    if (students) { req.body.students = students.map(student => student._id) }
    await new Course(req.body).save()
    res.status(200).send('New course successfully added!')
  } catch(err) { res.status(400).send(err.message) }
})

routes.delete('/delete', async(req, res) => {
  try {
    // Model.findById()，返回一个 doc
    const course = await Course.findById(req.query._id).exec()
    if (course) {
      await course.remove()
      res.status(200).send('Course deleted')
    } else { res.status(404).send('Error: Course not found') }
  } catch(err) { res.status(400).send(err.message) }
})

routes.put('/update', async(req, res) => {
  try {
    await Course.update({ _id: req.body._id }, req.body).exec()
    res.status(200).send('Course updated')
  } catch(err) { res.status(400).send(err.message) }
})

routes.get('/getCourse', async(req, res) => {
  try {
    const course = await Course.findById(req.query._id).exec()
    course ? res.send(JSON.stringify(course)) : res.status(404).send('Error: Course not found')
  } catch(err) { res.status(400).send(err.message) }
})

routes.get('/getCourses', async(req, res) => {
  try {
    const courses = await Course.find().exec()
    courses ? res.send(JSON.stringify(courses)) : res.status(404).send('Error: Courses not found')
  } catch(err) { res.status(400).send(err.message) }
})

routes.get('/getCoursesInfo', async(req, res) => {
  try {
    const courses = await Course.find({}, '_id name desc available').sort('date').exec()
    courses ? res.send(JSON.stringify(courses)) : res.status(404).send('Error: Courses not found')
  } catch(err) { res.status(400).send(err.message) }
})

// 家长端，获取所有笔记
routes.get('/getCourseNote', async(req, res) => {
  try {
    const course = await Course.findOne({ _id: req.query._id }, '-_id notes').exec()
    course ? res.send(JSON.stringify(course)) : res.status(404).send('Error: Note not found')
  } catch(err) { res.status(400).send(err.message) }
})

module.exports = routes
