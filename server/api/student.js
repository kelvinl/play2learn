// 学生接口
const express = require('express')
const routes = express.Router()
const { Student, Course } = require('./models')

routes.post('/add', async(req, res) => {
  try {
    const student = await new Student(req.body).save()
    await Course.update({}, { $push: { students: student._id } }, { multi: true }).exec()
    res.status(200).send('New student successfully added!')
  } catch(err) { res.status(400).send(err.message) }
})

routes.delete('/delete', async(req, res) => {
  try {
    // Model.findOne()，返回一个 doc
    const student = await Student.findById(req.query._id).exec()
      // doc 才有 remove()
    if (student) {
      await student.remove()
      res.status(200).send('Student deleted')
    } else { res.status(404).send("Can't find student") }
  } catch(err) { res.status(400).send(err.message) }
})

routes.put('/update', async(req, res) => {
  try {
    await Student.update({ _id: req.body._id }, req.body).exec()
    res.status(200).send('Student updated')
  } catch(err) { res.status(400).send(err.message) }
})

routes.get('/getStudent', async(req, res) => {
  try {
    const student = await Student.findById(req.query._id).exec()
    student ? res.send(JSON.stringify(student)) : res.status(404).send('Student not found')
  } catch(err) { res.status(400).send(err.message) }
})

routes.get('/getStudents', async(req, res) => {
  try {
    const students = await Student.find().exec()
    students ? res.send(JSON.stringify(students)) : res.status(404).send('Student not found')
  } catch(err) { res.status(400).send(err.message) }
})

module.exports = routes
